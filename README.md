# web server

This is part of preservation system for High Performance Computing Systems in Baden-Wuerttemberg. Was developed along CiTAR project as one of part of it.

Web-interface for

## Getting Started

These instructions will give you information HOWTO setup and run application. How to setup python environment.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing


1. Install Docker. How to do it, can be found at:
  for Ubuntu 18.04:
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04

2. Install PostgreSQL. How to do it, can be found at:
  for Ubuntu 18.04:
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04


## Deployment

Supervisor settings:
```
[program:web-server]
directory={path_to_dir}/web-server
command={path_to_dir}/web-env/bin/gunicorn web-server.wsgi:application
autostart=true
autorestart=true
stderr_logfile=/var/log/web-server/web-server.err.log
stdout_logfile=/var/log/web-server/web-server.out.log
```



## Authors

* **Kyryll Udod** - *Developer* - [kirilludod](kirilludod)

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
