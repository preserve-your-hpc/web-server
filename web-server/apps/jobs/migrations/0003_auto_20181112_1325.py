# Generated by Django 2.1.2 on 2018-11-12 13:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0002_createcontainerhistory'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobs',
            name='command',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='jobs',
            name='libraries',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='createcontainerhistory',
            name='job_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobs.Jobs'),
        ),
        migrations.AlterModelTable(
            name='createcontainerhistory',
            table='cointainer_history',
        ),
    ]
