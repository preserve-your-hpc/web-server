from django.contrib import admin
from django import forms

from .models import ContainerCreationTask, Jobs, Statuses


class ContainerCreationTaskAdmin(admin.ModelAdmin):
    readonly_fields=('uid', 'job', 'email', 'container_url')

# Register your models here.
admin.site.register(ContainerCreationTask, ContainerCreationTaskAdmin)
admin.site.register(Jobs)
admin.site.register(Statuses)
