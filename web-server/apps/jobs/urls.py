from django.urls import include, path
from jobs.views import JobsListView, JobDetailView, CreateContainerView, ContainerDetailView, UploadContainerView

import debug_toolbar

app_name = 'jobs'

urlpatterns = [

    path('', JobsListView.as_view(), name='index'),
    path('__debug__/', include(debug_toolbar.urls)),
    path('<job_id>/', JobDetailView.as_view(), name='job-detail'),
    path('get-container/<uid>/', ContainerDetailView.as_view(), name='container-detail'),
    path('create-container/<job_id>/', CreateContainerView.as_view(), name='create-container'),
    path('upload-container/<uid>/', UploadContainerView.as_view(), name='upload-container'),

]
