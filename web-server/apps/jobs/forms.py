from django import forms

from .models import ContainerCreationTask, Jobs
from libraries.models import Library

class CreateContainerForm(forms.ModelForm):
    CONTAINER_TYPE_CHOICES = (
        (1, 'Docker'),
        (2, 'Singularity'),
        # (0, 'RunC'),
    )

    # email = forms.EmailField(help_text='A valid email address, please.')
    container_type = forms.ChoiceField(choices=CONTAINER_TYPE_CHOICES)
    job_id = forms.CharField(widget=forms.HiddenInput)
    uid = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = ContainerCreationTask
        fields = ('email', 'container_type', 'job_id', 'uid', 'libs')

    def __init__(self, *args, **kwargs):
        super(CreateContainerForm, self).__init__(*args, **kwargs)
        self.fields['libs'].queryset = Library.objects.filter(status=1)


class ImportContainerForm(forms.Form):
    image_name = forms.CharField(initial='', required=False)
    runtime_type = forms.CharField(initial='singularity')
    process_args = forms.CharField(initial='ls')
    process_env = forms.CharField(initial='', required=False)
    input_folder = forms.CharField(initial='/input')
    output_folder = forms.CharField(initial='/output')
    image_type = forms.CharField(initial='simg')

    author = forms.CharField(initial='ku')
    description = forms.CharField(initial='test_descr')
    title = forms.CharField(initial='test_title')
