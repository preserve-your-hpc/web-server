import uuid
import requests

from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic import DetailView, FormView, ListView, View
from django.shortcuts import render, redirect

from .models import Jobs, ContainerCreationTask, Statuses
from .forms import CreateContainerForm, ImportContainerForm

from libraries.models import Library

class JobsListView(ListView):
    """View for list with all jobs history."""
    model = Jobs
    paginate_by = 100  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class JobDetailView(DetailView):
    """View for job's detail. Can be obtain by job_id."""
    model = Jobs
    slug_url_kwarg = 'job_id'
    slug_field = 'job_id'

    template_name = 'jobs/jobs_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        s1 = context.get('object').libraries.replace(']','').replace('[','')
        libs = s1.replace('"','').split(",")

        new_libs = {}
        for el in libs:
            name = el.split('/')[1]
            version = el.split('/')[2]
            category = el.split('/')[0]
            lib = Library.objects.filter(name=name, version=version)
            if lib:
                status = lib.first().status
            else:
                status = 'not available'
            new_libs[el] = status

        context.get('object').libraries = new_libs
        context['containers'] = ContainerCreationTask.objects.filter(job__job_id = context.get('object').job_id).order_by('-created')
        return context


class CreateContainerView(FormView):
    """
    Form view for creation container. Use for check container type,
    user's email (for notification) etc.
    """
    model = Jobs

    form_class = CreateContainerForm
    template_name = 'jobs/form.html'
    success_url = '/test/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        s1 = Jobs.objects.get(job_id = self.kwargs['job_id']).libraries.replace(']','').replace('[','')

        # libs = s1.replace('"','').split(",")
        #
        new_libs = []
        # for el in libs:
        #     name = el.split('/')[1]
        #     version = el.split('/')[2]
        #     category = el.split('/')[0]
        #     lib = Library.objects.filter(name=name, version=version, status=1)
        #     if lib:
        #         print(lib)
        #         new_libs.append(el)
        #
        # context['libraries'] = new_libs
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['job_id'] = self.kwargs['job_id']
        initial['uid'] = str(uuid.uuid4())
        return initial

    def form_invalid(self, form):
        return super().form_invalid(form)

    def form_valid(self, form):
        temp = ContainerCreationTask(job = Jobs.objects.get(job_id = form.cleaned_data.get('job_id','')),
                                     uid=form.cleaned_data.get('uid',''),
                                     status=Statuses.objects.get(id=1),
                                     email=form.cleaned_data.get('email',''),
                                     container_type=form.cleaned_data.get('container_type','0'))
        temp.save()
        for el in form.cleaned_data['libs']:
            temp.libs.add(el)
        temp.save()
        data = {"uid": form.cleaned_data.get('uid','')}
        r = requests.post('http://134.60.152.82/restapi/create-container/', data=data)

        if r.status_code == 200:
            return HttpResponseRedirect(reverse('jobs:job-detail', kwargs={'job_id': self.kwargs['job_id']}))
        else:
            raise Http404

class ContainerDetailView(DetailView):
    """View for container's detail. Can be obtain by uid."""
    model = ContainerCreationTask
    slug_url_kwarg = 'uid'
    slug_field = 'uid'

    template_name = 'jobs/container_detail.html'


class UploadContainerView(FormView):
    """Send request for uploading container to EAAS server."""
    form_class = ImportContainerForm
    template_name = 'jobs/import_form.html'

    def form_valid(self, form):
        import json
        data = {"uid": self.kwargs['uid']}
        data.update(form.cleaned_data)
        r = requests.post('http://134.60.152.82/restapi/export-container/', data=data)

        if r.status_code == 200:
            return HttpResponseRedirect(reverse('jobs:container-detail', kwargs={'uid': self.kwargs['uid']}))
        else:
            raise Http404
