from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import JSONField

from libraries.models import Library

class Jobs(models.Model):
    data = JSONField(blank=True, null=True)  # This field type is a guess.
    job_id = models.IntegerField(unique=True, blank=True, null=True)
    libraries = models.TextField(blank=True, null=True, default='')
    command = models.TextField(blank=True, null=True, default='')

    class Meta:
        db_table = 'jobs'

    def __str__(self):
        return '{}'.format(self.job_id)

    def __unicode__(self):
        return '{}'.format(self.job_id)


class Statuses(models.Model):
    name = models.CharField(unique=True, max_length=50, blank=True, null=True)
    description = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        db_table = 'statuses'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class ContainerCreationTask(models.Model):
    uid = models.CharField(max_length=100, null=True, blank=True, unique=True)
    job = models.ForeignKey('Jobs', on_delete=models.CASCADE)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    libs = models.ManyToManyField(Library)
    status = models.ForeignKey('Statuses', models.DO_NOTHING, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    container_url = models.CharField(max_length=200, blank=True, null=True)
    container_type = models.CharField(max_length=1, default='0')

    @property
    def ready(self):
        if self.status.name == "created":
            return True
        return False

    @property
    def upload(self):
        if self.container_type == "2":
            return True
        return False

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(ContainerCreationTask, self).save(*args, **kwargs)

    class Meta:
        ## FIXME: change to container
        db_table = 'container_history'

    def __str__(self):
        return '{}'.format(self.uid)

    def __unicode__(self):
        return '{}'.format(self.uid)
