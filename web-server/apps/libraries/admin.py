from django.contrib import admin

from .models import LibraryStatus, LibraryType, LibraryCategory, Cluster, Library

class LibraryAdmin(admin.ModelAdmin):
    raw_id_fields = ("cluster",)

# Register your models here.
admin.site.register(LibraryStatus)
admin.site.register(LibraryType)
admin.site.register(LibraryCategory)
admin.site.register(Cluster)
admin.site.register(Library)
