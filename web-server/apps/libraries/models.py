from django.db import models


class LibraryStatus(models.Model):
    """Model for library status (available or not)."""
    name = models.CharField(max_length=20, unique=True)

    class Meta:
        db_table = 'library_statuses'
        verbose_name_plural = "Library statuses"

    def __str__(self):
        return self.name

class LibraryType(models.Model):
    """Model for library type. Licences etc."""
    name = models.CharField(max_length=20, unique=True)

    class Meta:
        db_table = 'library_types'
        verbose_name_plural = "Library types"

    def __str__(self):
        return self.name

class LibraryCategory(models.Model):
    """Model for library category. Parsed from BwHPC."""
    name = models.CharField(max_length=50, unique=True)
    short_name = models.CharField(max_length=20)

    class Meta:
        db_table = 'library_categories'
        verbose_name_plural = "Library categories"

    def __str__(self):
        return self.name

class Cluster(models.Model):
    """Model for clusters from BWHPC."""
    name = models.CharField(max_length=20, unique=True)

    class Meta:
        db_table = 'clusters'
        verbose_name_plural = "clusters"

    def __str__(self):
        return self.name

class Library(models.Model):
    """Model for library. Contain all important information about library"""
    name = models.CharField(max_length=20)
    description = models.TextField(null=True)
    version = models.TextField(null=True)
    url = models.TextField(null=True)
    docker_directives = models.TextField(null=True)

    type = models.ForeignKey(LibraryType, default=1, on_delete=models.CASCADE)
    status = models.ForeignKey(LibraryStatus, default=1, on_delete=models.CASCADE)
    category = models.ForeignKey(LibraryCategory, default=1, on_delete=models.CASCADE)
    cluster = models.ManyToManyField(Cluster)

    class Meta:
        db_table = 'libraries'
        verbose_name_plural = "Libraries"

    def __str__(self):
        return self.name
